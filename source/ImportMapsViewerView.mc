import Toybox.Graphics;
import Toybox.WatchUi;
using Toybox.Timer;

class ImportMapsViewerView extends WatchUi.View {

    private var waitString;
	private var receivedString;
    private var errorString;
    private var count=0;
    private var displayLoader = null;
	private var downloadString;

    function initialize() {
        View.initialize();
    }

    // Load your resources here
    function onLayout(dc as Dc) as Void {
        waitString =  WatchUi.loadResource(Rez.Strings.wait);
    	receivedString =  WatchUi.loadResource(Rez.Strings.received);
        errorString =  WatchUi.loadResource(Rez.Strings.error);
		downloadString =  WatchUi.loadResource(Rez.Strings.download);
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_WHITE);
        dc.clear();
        
    	if(status!= :WAIT && status!= :DOWNLOAD_FIT ){
    		if(displayLoader!=null){
    		   	displayLoader.stop();
    		   	displayLoader=null;
    		}
            if(status == :SUCCESS){
    		    dc.drawText(dc.getWidth()/2, dc.getHeight()/2, Graphics.FONT_MEDIUM, receivedString, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
            }else{
                dc.drawText(dc.getWidth()/2, dc.getHeight()/2, Graphics.FONT_MEDIUM, errorString, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
            }
    	}else{
    		if(displayLoader==null){
    			displayLoader = new Timer.Timer();
    		   	displayLoader.start(method(:refresh), 75, true);
    		}
			if(status== :DOWNLOAD_FIT){
    	    	dc.drawText(dc.getWidth()/2, dc.getHeight()/2, Graphics.FONT_MEDIUM, downloadString, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
			}else{
				dc.drawText(dc.getWidth()/2, dc.getHeight()/2, Graphics.FONT_MEDIUM, waitString, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
			}
			var arc_width= 4;
    	    
    	    var max=25;
    	    if(count<25){
    	    	max=count;
    	    }else if(count>=100){
    	    	max=125-count;
    	    }
    	    for( var i = 0; i < max; i++) {
    	    	if(count<25){
       				drawIndicator(dc, dc.getWidth()/2-1,dc.getHeight()/2-1,i*50/100*-2*Math.PI/50, dc.getHeight()/2-arc_width-1,arc_width);
       			}else if(count>100){
       				drawIndicator(dc, dc.getWidth()/2-1,dc.getHeight()/2-1,(100-i)*50/100*-2*Math.PI/50, dc.getHeight()/2-arc_width-1,arc_width);
       			}else{
       				drawIndicator(dc, dc.getWidth()/2-1,dc.getHeight()/2-1,((count-25)+i)*50/100*-2*Math.PI/50, dc.getHeight()/2-arc_width-1,arc_width);
       			}
       		}
    	}
    }

    private function drawIndicator(dc, center_x, center_y, radian, radius,arc_width) {
		var xy = pol2Cart(center_x, center_y, radian, radius);
		dc.fillCircle(xy[0], xy[1], arc_width);
	}
    
	private function pol2Cart(center_x, center_y, radian, radius) {
		var x = center_x - radius * Math.sin(radian);
		var y = center_y - radius * Math.cos(radian);
		 
		return [Math.ceil(x), Math.ceil(y)];
	}

    function refresh(){
		count=count+1;
       	if(count>=124){
       		count=1;
      	}
      	WatchUi.requestUpdate();
	}

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
        if(displayLoader!=null){
    	   	displayLoader.stop();
    	   	displayLoader=null;
   		}
    }

}
