import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;
import Toybox.Communications;

class MessageListener extends Communications.ConnectionListener {
    function initialize() {
    	System.println("MessageListener initialize");
        Communications.ConnectionListener.initialize();
    }

    function onComplete() {
        System.println("MessageListener onComplete");
    }

    function onError() {
        System.println("MessageListener onError");
    }
}
var status = :WAIT;
class ImportMapsViewerApp extends Application.AppBase {

    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {
        Communications.registerForPhoneAppMessages(method(:onPhone));  
    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
    }

    // Return the initial view of your application here
    function getInitialView() as Array<Views or InputDelegates>? {
        return [ new ImportMapsViewerView() ] as Array<Views or InputDelegates>;
    }

    function onPhone(msg) { 
        System.println("onPhone "+msg.data);
        if(msg.data.equals("SERVER_OK") && status != :DOWNLOAD_FIT){
            importGPX();
        }
    }

    function importGPX() {
        status = :DOWNLOAD_FIT;
        System.println("importGPX");
        try {
            Communications.makeWebRequest("http://127.0.0.1:22222/track.fit", {}, {:method => Communications.HTTP_REQUEST_METHOD_GET,:responseType => Communications.HTTP_RESPONSE_CONTENT_TYPE_FIT}, method(:onReceiveTrack) );
        } catch( ex ) {
            Communications.transmit("STOP_SERVER_ERROR", null, new MessageListener());
            status = :ERROR_COMMUNICATION;
        }
    }

    function onReceiveTrack(responseCode, downloads) {
        System.println("onReceiveTrack");
        Communications.transmit("STOP_SERVER", null, new MessageListener());
        if (responseCode == Communications.BLE_CONNECTION_UNAVAILABLE) {
            status= :ERROR_COMMUNICATION;
        }else if (responseCode != 200 || downloads == null) {
            status= :ERROR_DOWNLOAD;
        }else {
            var download = downloads.next();
            System.println("onReceiveTrack: " + (download == null ? null : download.getName() + "/" + download.getId()));
            status= :SUCCESS;
        }
    }
}

function getApp() as ViewerMapsTransferApp {
    return Application.getApp() as ViewerMapsTransferApp;
}